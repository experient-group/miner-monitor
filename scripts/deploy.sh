#!/usr/bin/env bash

# Run from scripts folder
cd ..

sam build --use-container
sam package --s3-bucket eg-deploys --region us-east-1  --profile eg
sam deploy --stack-name "miner-monitor" --capabilities CAPABILITY_IAM --region us-east-1 --s3-bucket eg-deploys --profile eg