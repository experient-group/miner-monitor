import os
import requests


def lambda_handler(event, context):
    """
    Finds and logs status of BTC miner from SlushPool API
    """
    # Lambda environment variables
    auth_token = os.environ['SLUSH_POOL_AUTH_TOKEN']

    # Make HTTP request to SlushPool API
    r = requests.get("https://slushpool.com/accounts/workers/json/btc/", headers={'SlushPool-Auth-Token': auth_token})
    # Log HTTP response as JSON
    print(r.json())
