import time
from random import *


def lambda_handler(event, context):
    """
    Emulates unhealthy status of BTC miner from SlushPool API for the 'LOW' state
    """
    
    
    print({
        'btc': {
            'workers': {
                'TJHarrison.V1': {
                    'state': "low",
                    'last_share': time.gmtime(),
                    'hash_rate_unit': "Gh/s",
                    'hash_rate_scoring': randint(1, 20), 
                    'hash_rate_5m': randint(1, 20),
                    'hash_rate_60m': randint(1, 20),
                    'hash_rate_24h': randint(1, 20),
                }
            }
        }
    })
