import time


def lambda_handler(event, context):
    """
    Emulates unhealthy status of BTC miner from SlushPool API for the 'DISABLED' or 'DIS' state
    """

    print({
        'btc': {
            'workers': {
                'TJHarrison.V1': {
                    'state': "dis",
                    'last_share': time.gmtime(),
                    'hash_rate_unit': "Gh/s",
                    'hash_rate_scoring': 0,
                    'hash_rate_5m': 0,
                    'hash_rate_60m': 0,
                    'hash_rate_24h': 0,
                }
            }
        }
    })
