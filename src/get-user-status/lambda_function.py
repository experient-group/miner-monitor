import os
import requests


def lambda_handler(event, context):
    """
    Finds and logs user status of BTC miner from SlushPool API
    """
    
    auth_token = os.environ['SLUSH_POOL_AUTH_TOKEN']  # Securely load auth token (so it isn't stored in code)

    # Make HTTP request to SlushPool API
    r = requests.get("https://slushpool.com/accounts/profile/json/btc/", headers={'SlushPool-Auth-Token': auth_token})
    # Log HTTP response as JSON
    print(r.json())

